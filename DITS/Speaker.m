//
//  Speaker.m
//  DITS
//
//  Created by Alexis Apuli on 09/08/2017.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import "Speaker.h"
#import <AVFoundation/AVFoundation.h>
@import FirebaseRemoteConfig;

static Speaker *_sharedInstance = nil;
static AVSpeechSynthesizer *synthesizer;

@interface Speaker() <AVSpeechSynthesizerDelegate>

@end

@implementation Speaker

+ (Speaker *)sharedInstance
{
    @synchronized([Speaker class])
    {
        if (!_sharedInstance)
            _sharedInstance = [[self alloc] init];
            synthesizer = [[AVSpeechSynthesizer alloc] init];
            synthesizer.delegate = (id)self;
        return _sharedInstance;
    }
    
    return nil;
}

- (void)fetchConfigs {
    
    FIRRemoteConfig *remoteConfig = [FIRRemoteConfig remoteConfig];
    [remoteConfig setDefaults:@{
                                WELCOME_MSG_KEY : WELCOME_MSG,
                                LOGIN_MSG_FAILED_KEY : LOGIN_MSG_FAILED
                                }];

    FIRRemoteConfigSettings *remoteConfigSettings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
    remoteConfig.configSettings = remoteConfigSettings;

    [remoteConfig fetchWithCompletionHandler:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        if (error == nil) {
            NSLog(@"Remote config item is fetched");
            [remoteConfig activateFetched];
        } else {
            NSLog(@"%@",error.localizedDescription);
        }
        
    }];

}

- (void)speakText:(NSString *)text rate:(float)rate
{

}

- (void)speakText:(NSString *)text {
    
    AVSpeechUtterance *utter;
    utter = [AVSpeechUtterance speechUtteranceWithString:text];
   
    [synthesizer speakUtterance:utter];
    
}

- (void)speakTextForKey:(NSString *)key {
    FIRRemoteConfig *remoteConfig = [FIRRemoteConfig remoteConfig];
    NSLog(@"speaking %@",[[remoteConfig configValueForKey:key] stringValue]);
    [self speakText:[[remoteConfig configValueForKey:key] stringValue]];

}

@end
