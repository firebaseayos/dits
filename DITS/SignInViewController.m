//
//  Copyright (c) 2016 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "SignInViewController.h"
#import "UIViewController+Alerts.h"
#import "Speaker.h"

@import Firebase;

@interface SignInViewController () <UITextFieldDelegate>
@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *passTxt;


@property(strong, nonatomic) FIRAuthStateDidChangeListenerHandle handle;


@end

@implementation SignInViewController

- (void)viewDidLoad {
  [super viewDidLoad];

    self.image.layer.cornerRadius = self.image.frame.size.width/2;
    self.image.clipsToBounds = YES;

    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, _emailTxt.frame.size.height - borderWidth, _emailTxt.frame.size.width, _emailTxt.frame.size.height);
    border.borderWidth = borderWidth;
    [_emailTxt.layer addSublayer:border];
    _emailTxt.layer.masksToBounds = YES;
    
    CALayer *border2 = [CALayer layer];
    CGFloat borderWidth2 = 2;
    border2.borderColor = [UIColor whiteColor].CGColor;
    border2.frame = CGRectMake(0, _passTxt.frame.size.height - borderWidth2, _passTxt.frame.size.width, _passTxt.frame.size.height);
    border2.borderWidth = borderWidth2;
    [_passTxt.layer addSublayer:border2];
    _passTxt.layer.masksToBounds = YES;
    
    [GIDSignIn sharedInstance].uiDelegate = self;
//    [[GIDSignIn sharedInstance] signInSilently];
    [[GIDSignIn sharedInstance] signOut];
    [[FIRAuth auth] signOut:nil];
    self.handle = [[FIRAuth auth]
                 addAuthStateDidChangeListener:^(FIRAuth *_Nonnull auth, FIRUser *_Nullable user) {
                   if (user) {
                       [self dismissViewControllerAnimated:NO completion:nil];
                   } else {
                       [[Speaker sharedInstance] speakTextForKey:WELCOME_MSG_KEY];
                   }
                 }];
//    NSError *signOutError;
//    BOOL status = [[FIRAuth auth] signOut:&signOutError];
//    if (!status) {
//        NSLog(@"Error signing out: %@", signOutError);
//        return;
//    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _passTxt) {
        [_passTxt resignFirstResponder];
    }
    return YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_passTxt resignFirstResponder];
}

- (void)dealloc {
  [[FIRAuth auth] removeAuthStateDidChangeListener:_handle];
}
- (IBAction)loginBtn:(id)sender {
    [self showSpinner:^{
        // [START headless_email_auth]
        [[FIRAuth auth] signInWithEmail:_emailTxt.text
                               password:_passTxt.text
                             completion:^(FIRUser *user, NSError *error) {
                                 // [START_EXCLUDE]
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self hideSpinner:nil];
                                     if (error) {
                                         [[Speaker sharedInstance] speakTextForKey:LOGIN_MSG_FAILED_KEY];
                                         //                                         });
                                         [self showMessagePrompt: error.localizedDescription];
                                         return;
                                     }else{
                                         [FIRAnalytics logEventWithName:kFIREventLogin
                                                             parameters:@{
                                                                          kFIRParameterItemID:[NSString stringWithFormat:@"Signed a user with an email address and password"],
                                                                          kFIRParameterItemName:@"",
                                                                          kFIRParameterContentType:@""
                                                                          }];
                                         [self speakSalutationsForUser:user];
                                         [self dismissViewControllerAnimated:NO completion:nil];
                                         return;
                                     }
                                     
                                 });                                     //[self.navigationController popViewControllerAnimated:YES];
                                 
                                 // [END_EXCLUDE]
                             }];
        //[self performSegueWithIdentifier:@"ShowTab" sender:self];
        // [END headless_email_auth]
    }];
    
}
- (IBAction)registerBtn:(id)sender {

    [[Speaker sharedInstance] speakText:@"Hi, new? user?"];
    
    [self showTextInputPromptWithMessage:@"Email:"
       completionBlock:^(BOOL userPressedOK, NSString *_Nullable email) {
           if (!userPressedOK || !email.length) {
               return;
           }
           /*[self unwindForSegue:<#(nonnull UIStoryboardSegue *)#> towardsViewController:<#(nonnull UIViewController *)#>]*/
           
           [self showPasswordInputPromptWithMessage:@"Password:"
               completionBlock:^(BOOL userPressedOK, NSString *_Nullable password) {
                   if (!userPressedOK || !password.length) {
                       return;
                   }
                   
                   [self showSpinner:^{
                       // [START create_user]
                       [[FIRAuth auth] createUserWithEmail:email
                               password:password
                             completion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
                                 // [START_EXCLUDE]
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self hideSpinner:nil];
                                     if (error) {
                                         [[Speaker sharedInstance] speakTextForKey:LOGIN_MSG_FAILED_KEY];
                                         //                                         });
                                         [self showMessagePrompt: error.localizedDescription];
                                         return;
                                     }else{
                                         [FIRAnalytics logEventWithName:kFIREventLogin
                                                             parameters:@{
                                                                          kFIRParameterItemID:[NSString stringWithFormat:@"Signed a user with an email address and password"],
                                                                          kFIRParameterItemName:@"",
                                                                          kFIRParameterContentType:@""
                                                                          }];
                                         [self speakSalutationsForUser:user];
                                         [self dismissViewControllerAnimated:NO completion:nil];
                                         return;
                                     }

                                 });
                                 
                                 //NSLog(@"%@ created", user.email);
                                     //[self.navigationController popViewControllerAnimated:YES];
                              
                                 // [END_EXCLUDE]
                             }];
                       // [END create_user]
                   }];
               }];
       }];
    
    
}

- (void)speakSalutationsForUser:(FIRUser *)user {
    NSLog(@"%@",[[user.email componentsSeparatedByString:@"@"] firstObject]);
    [[Speaker sharedInstance] speakText:[NSString stringWithFormat:LOGIN_MSG, [[user.email componentsSeparatedByString:@"@"] firstObject]]];
}



@end
