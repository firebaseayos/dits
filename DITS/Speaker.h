//
//  Speaker.h
//  DITS
//
//  Created by Alexis Apuli on 09/08/2017.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import <Foundation/Foundation.h>

#define WELCOME_MSG         @"Welcome to D.I.T.S, your tracking assistant, To Start, Please Login"
#define LOGIN_MSG           @"Hi %@, what are you borrowing today"
#define LOGIN_MSG_FAILED    @"Seems? like you've entered invalid credentials"
#define BARCODE_DETECTED    @"Do you want to borrow this device"

#define WELCOME_MSG_KEY         @"WELCOME_KEY"
#define LOGIN_MSG_KEY           @"LOGIN_MSG_KEY"
#define LOGIN_MSG_FAILED_KEY    @"LOGIN_MSG_FAILED_KEY"

@interface Speaker : NSObject
+(Speaker *)sharedInstance;
- (void)speakText:(NSString *)text;
- (void)speakTextForKey:(NSString *)key;
- (void)fetchConfigs;
@end
