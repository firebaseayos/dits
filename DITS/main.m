//
//  main.m
//  DITS
//
//  Created by Alexis Apuli on 09/08/2017.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
