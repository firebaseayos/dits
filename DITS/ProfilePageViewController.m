//
//  ProfilePageViewController.m
//  DITS
//
//  Created by Hazel Micua on 8/10/17.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import "ProfilePageViewController.h"
@import Firebase;

@interface ProfilePageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgvOne;
@property (weak, nonatomic) IBOutlet UIImageView *imgvTwo;
@property (weak, nonatomic) IBOutlet UIImageView *imgvThree;
@property (weak, nonatomic) IBOutlet UIImageView *imgvFour;

@end

@implementation ProfilePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Create a reference to the file you want to download
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *gsReference1 = [storage referenceForURL:@"gs://dits-1e4.appspot.com/alex.jpg"];

    
    // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
    [gsReference1 dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData *data, NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
        } else {
            // Data for "images/island.jpg" is returned
            UIImage *one = [UIImage imageWithData:data];
            self.imgvOne.image = one;
        }
    }];
    
    FIRStorageReference *gsReference2 = [storage referenceForURL:@"gs://dits-1e4.appspot.com/chiz.jpg"];
    
    
    // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
    [gsReference2 dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData *data, NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
        } else {
            // Data for "images/island.jpg" is returned
            UIImage *two = [UIImage imageWithData:data];
            self.imgvTwo.image = two;
        }
    }];
    
    FIRStorageReference *gsReference3 = [storage referenceForURL:@"gs://dits-1e4.appspot.com/hazel.jpg"];
    
    
    // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
    [gsReference3 dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData *data, NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
        } else {
            // Data for "images/island.jpg" is returned
            UIImage *three = [UIImage imageWithData:data];
            self.imgvThree.image = three;
        }
    }];
    
    FIRStorageReference *gsReference4 = [storage referenceForURL:@"gs://dits-1e4.appspot.com/patricia.jpg"];
    
    
    // Download in memory with a maximum allowed size of 1MB (1 * 1024 * 1024 bytes)
    [gsReference4 dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData *data, NSError *error){
        if (error != nil) {
            // Uh-oh, an error occurred!
        } else {
            // Data for "images/island.jpg" is returned
            UIImage *four = [UIImage imageWithData:data];
            self.imgvFour.image = four;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
