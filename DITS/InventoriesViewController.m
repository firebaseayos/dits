//
//  InventoriesViewController.m
//  Inventory
//
//  Created by Alexis Apuli on 7/26/17.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import "InventoriesViewController.h"
@import Firebase;
@import Firestore;
@interface InventoriesViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arr;
}
@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@end

@implementation InventoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arr = [NSMutableArray new];
}

- (void)viewDidAppear:(BOOL)animated {
    FIRUser *user = [FIRAuth auth].currentUser;
    
    [arr removeAllObjects];
    [_mainTable reloadData];
    
    if (user) {
        FIRCollectionReference *ref = [[FIRFirestore firestore] collectionWithPath:user.uid];
        [ref getDocumentsWithCompletion:^(FIRQuerySnapshot * _Nullable snapshot, NSError * _Nullable error) {
            [arr addObjectsFromArray:snapshot.documents];
            [_mainTable reloadData];
        }];
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cellIdentifier"];
    }
    
    
    FIRDocumentSnapshot *document = [arr objectAtIndex:indexPath.row];
    NSDictionary *details = document.data;
    
    if(indexPath.row % 2 == 0)
    {
        cell.backgroundColor =[UIColor colorWithRed:255.0f/255.0f green:233.0f/255.0f blue:220.0f/255.0f alpha:0.5];
        
    }else{
        cell.backgroundColor =[UIColor colorWithRed:230.0f/255.0f green:169.0f/255.0f blue:163.0f/255.0f alpha:0.5];
    }
    
    if (details[@"returnDate"]) {
        
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        [dateFormat setDateFormat: @"EEEE, MMM d, h:mm a"];
        NSString *dateString = [dateFormat stringFromDate:details[@"returnDate"]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"Returned last %@",dateString];
        
        cell.textLabel.textColor = [UIColor colorWithRed:117.0f/255.0f green:99.0f/255.0f blue:89.0f/255.0f alpha:1];
    }

    if (details[@"borrowedDate"]) {
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        [dateFormat setDateFormat: @"EEEE, MMM d, h:mm a"];
        NSString *dateString = [dateFormat stringFromDate:details[@"borrowedDate"]];
        
        
        cell.textLabel.text = [NSString stringWithFormat:@"Borrowed on %@",dateString];
        
        cell.textLabel.textColor = [UIColor colorWithRed:117.0f/255.0f green:99.0f/255.0f blue:89.0f/255.0f alpha:1];
    }
    
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Device: %@",details[@"deviceCode"]];
    cell.detailTextLabel.textColor = [UIColor colorWithRed:193.0f/255.0f green:129.0f/255.0f blue:114.0f/255.0f alpha:1];
    
    return cell;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
