//
//  ScanViewController.m
//  Inventory
//
//  Created by Alexis Apuli on 7/26/17.
//  Copyright © 2017 Alexis Apuli. All rights reserved.
//

#import "ScanViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "Speaker.h"
#import "SignInViewController.h"
@import Firestore;
@import Firebase;


@interface ScanViewController () <AVCaptureMetadataOutputObjectsDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    UIView *_highlighter;
}

@property (weak, nonatomic) IBOutlet UIView *scannerView;
@property (weak, nonatomic) IBOutlet UILabel *deviceString;
@property (strong, nonatomic) FIRDatabaseReference *ref;
@property (strong, nonatomic) NSString *deviceCode;
@property (strong, nonatomic) NSString *userEmail;
@property (strong, nonatomic) NSString *userUID;
@property (strong, nonatomic) NSString *timestamp;
@property (nonatomic) BOOL deviceAssigned;

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.ref = [[FIRDatabase database] reference];
    [self setupScanner];
    [self logoutBtn];
    
 
    
    [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
}

-(void) viewWillAppear:(BOOL)animated{
    
    FIRUser *user = [FIRAuth auth].currentUser;
    
    if (!user) {
        
    }else{
        [[[_ref child:@"users"] child:(user.uid)] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            if([snapshot.value isKindOfClass:[NSNull class]] || snapshot.value == nil) {
                return;
            }
                
            self.userEmail = snapshot.value[@"email"];
            self.userUID = user.uid;
            self.timestamp = snapshot.value[@"date"];
            self.deviceCode = snapshot.value[@"deviceCode"];
            self.deviceAssigned = [snapshot.value[@"deviceAssigned"] boolValue] ;
            
        } withCancelBlock:^(NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
        }];
        
       
        
    }
    
}


- (void)delaySegue {
    [self.tabBarController performSegueWithIdentifier:@"showTab" sender:self.tabBarController];
    _highlighter.frame = CGRectZero;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupScanner {
    //setup highlighter
    _highlighter = [[UIView alloc] init];
    _highlighter.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin;
    _highlighter.layer.borderColor = [UIColor greenColor].CGColor;
    _highlighter.layer.borderWidth = 3;
    [self.scannerView addSubview:_highlighter];
    
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = CGRectMake(self.scannerView.bounds.origin.x, self.scannerView.bounds.origin.y, self.scannerView.bounds.size.width, self.scannerView.bounds.size.height);
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.scannerView.layer addSublayer:_prevLayer];
    
    [_session startRunning];
    [self.scannerView bringSubviewToFront:_highlighter];
}


#pragma mark - AVDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])
            {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString) {
            break;
        }
    }
    
        if (detectionString != nil)
        {
            if (_deviceAssigned) {
                [[Speaker sharedInstance] speakText:@"Do you want to return this device?"];
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle:[NSString stringWithFormat:@"BARCODE DETECTED! %@", detectionString]
                                            message:@"Do you want to return this device?"
                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *yesButton = [UIAlertAction
                                            actionWithTitle:@"YES"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                
                                                if ([detectionString isEqualToString:self.deviceCode]){
                                                    
                                                    if (_deviceAssigned) {
                                                        
                                                        NSDateFormatter *dateFormat = [NSDateFormatter new];
                                                        [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
                                                        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
                                                        
                                                        NSDictionary *post = @{@"deviceAssigned":@false,
                                                                               @"email": _userEmail,
                                                                               @"deviceCode": detectionString,
                                                                               @"date": dateString};
                                                        NSDictionary *childUpdates = @{[@"/users/" stringByAppendingString:_userUID]: post};
                                                        [self updateFirestore:post];
                                                        [[[FIRDatabase database] reference] updateChildValues:childUpdates withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                                            [[Speaker sharedInstance] speakText:@"Thank you for returning the device"];
                                                            UIAlertController *dismissAlert = [UIAlertController
                                                                                               alertControllerWithTitle:@""
                                                                                               message:[NSString stringWithFormat:@"Device %@ has been unassigned from %@ \n %@", detectionString, _userEmail, _timestamp]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                            UIAlertAction *dismissButton =[UIAlertAction
                                                                                           actionWithTitle:@"OK"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                               [_session startRunning];
                                                                                               [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
                                                                                           }];
                                                            [dismissAlert addAction:dismissButton];
                                                            [self presentViewController:dismissAlert animated:YES completion:nil];
                                                        }];
                                                    }else{
                                                        NSDateFormatter *dateFormat = [NSDateFormatter new];
                                                        [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
                                                        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
                                                        
                                                        NSDictionary *post = @{@"deviceAssigned":@true,
                                                                               @"email": _userEmail,
                                                                               @"deviceCode": detectionString,
                                                                               @"date": dateString};
                                                        NSDictionary *childUpdates = @{[@"/users/" stringByAppendingString:_userUID]: post};
                                                        [self updateFirestore:post];
                                                        [[[FIRDatabase database] reference] updateChildValues:childUpdates withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                                            [[Speaker sharedInstance] speakText:@"You have successfully borrowed the device"];
                                                            UIAlertController *dismissAlert = [UIAlertController
                                                                                               alertControllerWithTitle:@""
                                                                                               message:[NSString stringWithFormat:@"Device %@ has been successfully assigned to %@ \n %@", detectionString, _userEmail, _timestamp]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                            UIAlertAction *dismissButton =[UIAlertAction
                                                                                           actionWithTitle:@"OK"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                               [_session startRunning];
                                                                                               [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
                                                                                               
                                                                                           }];
                                                            [dismissAlert addAction:dismissButton];
                                                            [self presentViewController:dismissAlert animated:YES completion:nil];
                                                        }];
                                                    }
                                                }else{
                                                    [[Speaker sharedInstance] speakText:@"Oooooww, looks like this is not your device."];
                                                    UIAlertController *dismissAlert = [UIAlertController
                                                                                       alertControllerWithTitle:@"Oppps!"
                                                                                       message:[NSString stringWithFormat:@"Looks like this is not your device."]
                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *dismissButton =[UIAlertAction
                                                                                   actionWithTitle:@"OK"
                                                                                   style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action) {
                                                                                       [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                       [_session startRunning];
                                                                                   }];
                                                    [dismissAlert addAction:dismissButton];
                                                    [self presentViewController:dismissAlert animated:YES completion:nil];
                                                    _highlighter.frame = CGRectZero;
                                                }
                                                
                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                            }];
                
                UIAlertAction *noButton =[UIAlertAction
                                          actionWithTitle:@"NO"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              [_session startRunning];
                                              [[Speaker sharedInstance] speakText:@"Ok, what do you want to do then"];
                                              _highlighter.frame = CGRectZero;
                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                          }];
                [alert addAction:yesButton];
                [alert addAction:noButton];
                [_session stopRunning];
                [self presentViewController:alert animated:YES completion:nil];
//                break;
            } else {
                [[Speaker sharedInstance] speakText:@"Do you wish to borrow this device?"];
                UIAlertController *alert = [UIAlertController
                                            alertControllerWithTitle:[NSString stringWithFormat:@"BARCODE DETECTED! %@", detectionString]
                                            message:@"Do you wish to borrow this device?"
                                            preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *yesButton = [UIAlertAction
                                            actionWithTitle:@"YES"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                
                                                if ([detectionString isEqualToString:self.deviceCode]){
                                                    
                                                    if (_deviceAssigned) {
                                                        
                                                        NSDateFormatter *dateFormat = [NSDateFormatter new];
                                                        [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
                                                        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
                                                        
                                                        NSDictionary *post = @{@"deviceAssigned":@false,
                                                                               @"email": _userEmail,
                                                                               @"deviceCode": detectionString,
                                                                               @"date": dateString};
                                                        NSDictionary *childUpdates = @{[@"/users/" stringByAppendingString:_userUID]: post};
                                                        [self updateFirestore:post];
                                                        [[[FIRDatabase database] reference] updateChildValues:childUpdates withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                                            [[Speaker sharedInstance] speakText:@"Thank you for returning the device"];
                                                            UIAlertController *dismissAlert = [UIAlertController
                                                                                               alertControllerWithTitle:@""
                                                                                               message:[NSString stringWithFormat:@"Device %@ has been unassigned from %@ \n %@", detectionString, _userEmail, _timestamp]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                            UIAlertAction *dismissButton =[UIAlertAction
                                                                                           actionWithTitle:@"OK"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                               [_session startRunning];
                                                                                               [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
                                                                                           }];
                                                            [dismissAlert addAction:dismissButton];
                                                            [self presentViewController:dismissAlert animated:YES completion:nil];
                                                        }];
                                                    }else{
                                                        NSDateFormatter *dateFormat = [NSDateFormatter new];
                                                        [dateFormat setDateFormat: @"yyyy-MM-dd HH:mm:ss zzz"];
                                                        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
                                                        
                                                        NSDictionary *post = @{@"deviceAssigned":@true,
                                                                               @"email": _userEmail,
                                                                               @"deviceCode": detectionString,
                                                                               @"date": dateString};
                                                        NSDictionary *childUpdates = @{[@"/users/" stringByAppendingString:_userUID]: post};
                                                        [self updateFirestore:post];
                                                        [[[FIRDatabase database] reference] updateChildValues:childUpdates withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                                            [[Speaker sharedInstance] speakText:@"You have successfully borrowed the device"];
                                                            UIAlertController *dismissAlert = [UIAlertController
                                                                                               alertControllerWithTitle:@""
                                                                                               message:[NSString stringWithFormat:@"Device %@ has been successfully assigned to %@ \n %@", detectionString, _userEmail, _timestamp]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                                                            UIAlertAction *dismissButton =[UIAlertAction
                                                                                           actionWithTitle:@"OK"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                               [_session startRunning];
                                                                                               [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
                                                                                               
                                                                                           }];
                                                            [dismissAlert addAction:dismissButton];
                                                            [self presentViewController:dismissAlert animated:YES completion:nil];
                                                        }];
                                                    }
                                                }else{
                                                    [[Speaker sharedInstance] speakText:@"Oooooww, looks like this is not your device."];
                                                    UIAlertController *dismissAlert = [UIAlertController
                                                                                       alertControllerWithTitle:@"Oppps!"
                                                                                       message:[NSString stringWithFormat:@"Looks like this is not your device."]
                                                                                       preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *dismissButton =[UIAlertAction
                                                                                   actionWithTitle:@"OK"
                                                                                   style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action) {
                                                                                       [dismissAlert dismissViewControllerAnimated:YES completion:nil];
                                                                                       [_session startRunning];
                                                                                   }];
                                                    [dismissAlert addAction:dismissButton];
                                                    [self presentViewController:dismissAlert animated:YES completion:nil];
                                                    _highlighter.frame = CGRectZero;
                                                }
                                                
                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                            }];
                
                UIAlertAction *noButton =[UIAlertAction
                                          actionWithTitle:@"NO"
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * action) {
                                              [_session startRunning];
                                              [[Speaker sharedInstance] speakText:@"Ok, what do you want to do then"];
                                              _highlighter.frame = CGRectZero;
                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                          }];
                [alert addAction:yesButton];
                [alert addAction:noButton];
                [_session stopRunning];
                [self presentViewController:alert animated:YES completion:nil];
//                break;
            }
//        }
//        else
//            NSLog(@"Failed");
    }
    
    _highlighter.frame = highlightViewRect;
}


- (void)updateFirestore:(NSDictionary *)data {
    
    NSDictionary *deviceData = [NSDictionary dictionary];
    
    //device borrowed
    if ([data[@"deviceAssigned"] boolValue]) {
        deviceData = @{@"deviceCode" : data[@"deviceCode"], @"borrowedDate" : [FIRFieldValue fieldValueForServerTimestamp]};
    } else {
        //device returned
        deviceData = @{@"deviceCode" : data[@"deviceCode"], @"returnDate" : [FIRFieldValue fieldValueForServerTimestamp]};
    }
    NSLog(@"Saving in firestore %@\n%@\n%@",_userUID, data, deviceData);
    
    FIRCollectionReference *ref = [[FIRFirestore firestore] collectionWithPath:_userUID];
    [[ref documentWithAutoID] setData:deviceData];
    
}


- (void)logoutBtn {
    
    UIButton *overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [overlayButton setImage:[UIImage imageNamed:@"logout.ico"] forState:UIControlStateNormal];
    
    [overlayButton setFrame:CGRectMake(320, 570, 30, 30)];
    
    [overlayButton addTarget:self action:@selector(logoutButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [[self view] addSubview:overlayButton];
}

- (void) logoutButtonPressed {
    
    NSError *signOutError;
    
    BOOL status = [[FIRAuth auth] signOut:&signOutError];
    
    [[GIDSignIn sharedInstance] signOut];
    
    if (!status) {
        
        NSLog(@"Error signing out: %@", signOutError);
        
        return;
        
    }else{
        
        [self performSelector:@selector(delaySegue) withObject:nil afterDelay:0.01];
        
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
